/*******************************************************************************
 * Copyright 2020 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.heuristic.pbcd.subtrees;

import java.util.Collections;

import javax.xml.stream.XMLStreamException;

import org.jkarma.heuristic.utils.KarmaArgs;
import org.jkarma.heuristic.utils.Utils;
import org.jkarma.mining.joiners.TidSet;
import org.jkarma.mining.providers.BaseProvider;
import org.jkarma.mining.providers.TidSetProvider;
import org.jkarma.mining.structures.MiningStrategy;
import org.jkarma.mining.structures.Strategies;
import org.jkarma.mining.windows.WindowingStrategy;
import org.jkarma.mining.windows.Windows;
import org.jkarma.model.LabeledEdge;
import org.jkarma.model.TemporalGraph;
import org.jkarma.pbcd.descriptors.Descriptors;
import org.jkarma.pbcd.detectors.Detectors;
import org.jkarma.pbcd.detectors.PBCD;
import org.jkarma.pbcd.patterns.Patterns;
import org.jkarma.pbcd.similarities.Similarity;
import org.jkarma.pbcd.similarities.UnweightedJaccard;

public class MixedTanimotoDFS {

	public static PBCD<TemporalGraph, LabeledEdge, TidSet, Boolean> buildDetector(KarmaArgs args){
		//we build the provider
		WindowingStrategy<TidSet> windows = Windows.cumulativeSliding();
		BaseProvider<LabeledEdge, TidSet> provider = new TidSetProvider<LabeledEdge>(windows);
		
		//we build the similarity measure
		Similarity<Boolean> similarity = new UnweightedJaccard();
		
		//we assemble a limited dfs frequent subgraph mining strategy (ECLAT)
		MiningStrategy<LabeledEdge, TidSet> strategy = Strategies.uponSubtrees(Collections.emptySet())
			.limitDepth(args.getMaxDepth())
			.eclat(args.getMinSupport())
			.dfs(provider);
		
		//we assemble the detection strategy
		return Detectors.upon(strategy)
			.unweighted((p,s) -> Patterns.isFrequent(p, args.getMinSupport(), s), similarity)
			.describe(Descriptors.partialEps(args.getMinSupport(), args.getMinGr()))
			.build(args.getMinChange(), args.getBlockSize());
	}
	
	
	
	/**
	 * Application entry point, in this static method
	 * we perform command-line argument parsing and validation.
	 * If validation succeed, we launch the detector on the designated
	 * data stream.
	 * 
	 * @param args
	 * @throws XMLStreamException 
	 */
	public static void main(String[] args) throws XMLStreamException{
		try{
			Utils.parseArgsAndExecute(args, KarmaArgs.class, 
				Utils::getTemporalGraphStream, 
				MixedTanimotoDFS::buildDetector
			);
		}catch(InstantiationException | IllegalAccessException ex){
			ex.printStackTrace();
		}
	}
}
