/*******************************************************************************
 * Copyright 2020 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.heuristic.pbcd.subgraphs;

import java.util.Collections;

import javax.xml.stream.XMLStreamException;

import org.jkarma.heuristic.utils.BeamKarmaArgs;
import org.jkarma.heuristic.utils.Utils;
import org.jkarma.mining.heuristics.OddsRatioHeuristic;
import org.jkarma.mining.joiners.TidSet;
import org.jkarma.mining.providers.BaseProvider;
import org.jkarma.mining.providers.TidSetProvider;
import org.jkarma.mining.structures.MiningStrategy;
import org.jkarma.mining.structures.Strategies;
import org.jkarma.mining.windows.WindowingStrategy;
import org.jkarma.mining.windows.Windows;
import org.jkarma.model.LabeledEdge;
import org.jkarma.model.TemporalGraph;
import org.jkarma.pbcd.descriptors.Descriptors;
import org.jkarma.pbcd.detectors.Detectors;
import org.jkarma.pbcd.detectors.PBCD;
import org.jkarma.pbcd.patterns.Patterns;
import org.jkarma.pbcd.similarities.Similarity;
import org.jkarma.pbcd.similarities.WeightedJaccard;

public class LandmarkWJaccardBeamOddsRatio {

	public static PBCD<TemporalGraph, LabeledEdge, TidSet, Double> buildDetector(BeamKarmaArgs args){
		//we build the provider
		WindowingStrategy<TidSet> windows = Windows.cumulativeLandmark();
		BaseProvider<LabeledEdge, TidSet> provider = new TidSetProvider<LabeledEdge>(windows);
		
		//we build the similarity measure
		Similarity<Double> similarity = new WeightedJaccard();

		//we build the heuristic evaluation function
		OddsRatioHeuristic<LabeledEdge,TidSet> heuristic = new OddsRatioHeuristic<LabeledEdge,TidSet>(false);
		
		//we assemble a limited dfs frequent subgraph mining strategy (ECLAT)
		MiningStrategy<LabeledEdge, TidSet> strategy = Strategies.uponSubgraphs(Collections.emptySet())
			.limitDepth(args.getMaxDepth())
			.eclat(args.getMinSupport())
			.beam(provider, heuristic, args.getBeamSize());
		
		//we assemble the detection strategy
		return Detectors.upon(strategy)
			.weighted(Patterns::getRelativeFrequency, similarity)
			.describe(Descriptors.empty())
			.build(args.getMinChange(), args.getBlockSize());
	}
	
	
	
	/**
	 * Application entry point, in this static method
	 * we perform command-line argument parsing and validation.
	 * If validation succeed, we launch the detector on the designated
	 * data stream.
	 * 
	 * @param args
	 * @throws XMLStreamException 
	 */
	public static void main(String[] args) throws XMLStreamException{
		try{
			Utils.parseArgsAndExecute(args, BeamKarmaArgs.class, 
				Utils::getTemporalGraphStream, 
				LandmarkWJaccardBeamOddsRatio::buildDetector
			);
		}catch(InstantiationException | IllegalAccessException ex){
			ex.printStackTrace();
		}
	}
}
