/*******************************************************************************
 * Copyright 2020 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.heuristic.utils;

import java.io.File;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import org.kohsuke.args4j.Option;

public class KarmaArgs {
	
	@Option(name="-i", required=true, usage="path to a valid CSV input file")
	private File streamSource;
	
	@Option(name="-o", required=true, usage="path to the file that will contain the CSV results")
	private File outputFile;
	
	@Option(name="-t", required=true, usage="pattern maximum length (greater or equal than 1)")
	@Min(1)
	private int maximumDepth;
	
	@Option(name="-bs", required=true, usage="fixed block size (greater or equal than 1)")
	@Min(1)
	private int blockSize;
	
	@Option(name="-minSup", required=true, usage="minimum support threshold (between 0 and 1)")
	@Min(0) @Max(1)
	private float minimumSupport;
	
	@Option(name="-minMc", required=true, usage="minimum macroscopic change threshold (between 0 and 1)")
	@Min(0) @Max(1)
	private float minimumMacrochange;
	
	@Option(name="-minGr", required=true, usage="minimum microscopic change/growth-rate threshold (greater or equal than 1)")
	@Min(1)
	private float minimumGrowthRate;
	
	@Option(name="-p", required=false, usage="path to the file that will contain the patterns")
	private File patternFile;

	public File getStreamSource() {
		return streamSource;
	}

	public File getOutputFile() {
		return outputFile;
	}
	
	public File getPatternFile() {
		return patternFile;
	}

	public int getMaxDepth() {
		return maximumDepth;
	}

	public int getBlockSize() {
		return blockSize;
	}

	public float getMinSupport() {
		return minimumSupport;
	}

	public float getMinChange() {
		return minimumMacrochange;
	}

	public float getMinGr() {
		return minimumGrowthRate;
	}

}
