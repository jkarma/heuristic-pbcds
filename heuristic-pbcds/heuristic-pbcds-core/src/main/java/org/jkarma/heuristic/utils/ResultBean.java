/*******************************************************************************
 * Copyright 2020 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.heuristic.utils;

import com.univocity.parsers.annotations.Parsed;

public class ResultBean {
	
	@Parsed(index=0)
	public int changeDetected;
	
	@Parsed(index=1)
	public String startLandmark;
	
	@Parsed(index=2)
	public String endLandmark;
	
	@Parsed(index=3)
	public String startBlock;
	
	@Parsed(index=4)
	public String endBlock;
	
	@Parsed(index=5)
	public double changeAmount;
	
	@Parsed(index=6)
	public long epCount;
	
	@Parsed(index=7)
	public long jepCount;
	
	@Parsed(index=8)
	public double sumGrowthRate;
	
	@Parsed(index=9)
	public long totalTime;
	
	@Parsed(index=10)
	public long patternUpdateTime;
	
	@Parsed(index=11)
	public long patternFilterTime;
		
}
