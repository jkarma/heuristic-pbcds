/*******************************************************************************
 * Copyright 2020 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.heuristic.utils;

import org.kohsuke.args4j.Option;

public class BeamKarmaArgs extends KarmaArgs{

	@Option(name="-k", required=true, usage="beam-search size (only top k candidates will be selected)")
	private int beamSize;

	public int getBeamSize() {
		return this.beamSize;
	}
	
}