/*******************************************************************************
 * Copyright 2020 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.heuristic.pbcd.boilerplate;

import java.text.DecimalFormat;
import java.util.Collection;
import java.util.Vector;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.jkarma.heuristic.utils.KarmaArgs;
import org.jkarma.model.Transaction;
import org.jkarma.pbcd.datastreams.Block;
import org.jkarma.pbcd.datastreams.TimeWindow;
import org.jkarma.pbcd.detectors.PBCD;
import org.jkarma.pbcd.events.ChangeDescriptionCompletedEvent;
import org.jkarma.pbcd.events.ChangeDescriptionStartedEvent;
import org.jkarma.pbcd.events.ChangeDetectedEvent;
import org.jkarma.pbcd.events.ChangeNotDetectedEvent;
import org.jkarma.pbcd.events.PBCDEventListener;
import org.jkarma.pbcd.events.PatternUpdateCompletedEvent;
import org.jkarma.pbcd.events.PatternUpdateStartedEvent;
import org.jkarma.pbcd.patterns.Pattern;

public abstract class AbstractPBCDRunner<A extends Transaction<B>, B extends Comparable<B>, C, D, E extends KarmaArgs, F> 
implements Function<Stream<A>, Vector<F>> {

	public E args;
	protected PBCD<A,B,C,D> detector;

	protected long totalLatticeUpdateTime = 0;
	protected long totalDescriptionTime = 0;
	protected long changePointCount = 0;
	protected long totalPatternCount = 0;
	protected float totalMacroChange = 0;
	
	public AbstractPBCDRunner(E args, PBCD<A,B,C,D> pbcd) {
		if(args==null || pbcd==null) {
			throw new IllegalArgumentException();
		}
		this.args = args;
		this.detector = pbcd;
	}

	@Override
	public Vector<F> apply(Stream<A> stream) {
		Vector<F> results = new Vector<>();

		//we call the start routine
		this.start();
		
		//run only if a proper data stream has been created
		if(stream!=null && detector!=null) {
			//we listen for useful events
			this.detector.registerListener(
				new PBCDEventListener<B, C>() {

					private long totalTime = 0;
					private long updateTime = 0;
					private long filteringTime = 0;
					private DecimalFormat df = new DecimalFormat("#.####");
					private String mcMin = df.format(args.getMinChange() * 100);

					@Override
					public void patternUpdateStarted(PatternUpdateStartedEvent<B, C> event) {
						updateTime = System.currentTimeMillis();
						totalTime = updateTime;
					}

					@Override
					public void patternUpdateCompleted(PatternUpdateCompletedEvent<B, C> event) {
						updateTime = System.currentTimeMillis()-updateTime;
						totalLatticeUpdateTime += updateTime;		
					}

					@Override
					public void changeDescriptionStarted(ChangeDescriptionStartedEvent<B, C> event) {
						filteringTime = System.currentTimeMillis();	
					}

					@Override
					public void changeDescriptionCompleted(ChangeDescriptionCompletedEvent<B, C> event) {
						filteringTime = System.currentTimeMillis()-filteringTime;
						totalDescriptionTime += filteringTime;	
					}

					@Override
					public void changeDetected(ChangeDetectedEvent<B, C> event) {
						Collection<Pattern<B,C>> patterns = event.getDescription().collect(Collectors.toList());
						TimeWindow landmark = event.getLandmarkWindow();
						Block<?> block = event.getLatestBlock();
						double magnitude = event.getAmount();
						
						totalTime = System.currentTimeMillis()-totalTime;
						
						//we get the resulting bean
						F bean = getChangeDetectedBean(
							landmark, block, magnitude, patterns, totalTime, updateTime, filteringTime
						);
						
						//output cmd line message
						String percentage = this.df.format(magnitude * 100);
						System.out.println("block arrived: "+block+" (change: "+percentage+"% over "+mcMin+"% min)");
						
						//we append the result bean to the list of all the results
						results.addElement(bean);
					}

					@Override
					public void changeNotDetected(ChangeNotDetectedEvent<B, C> event) {
						TimeWindow landmark = event.getLandmarkWindow();
						Block<?> block = event.getLatestBlock();
						double magnitude = event.getAmount();
						
						totalTime = System.currentTimeMillis()-totalTime;
						
						//we get the resulting bean
						F bean = getChangeNotDetectedBean(
							landmark, block, magnitude, totalTime, updateTime, filteringTime
						);
						
						//output cmd line message
						String percentage = this.df.format(magnitude * 100);
						System.out.println("block arrived: "+block+" (change: "+percentage+"% over "+mcMin+"% min)");
						
						//we append the result bean to the list of all the results
						results.addElement(bean);
					}

				}
			);

			//we save the input params to file
			//this.saveParams();

			//we start the change detection phase
			System.out.println("stream started");
			long startTime = System.currentTimeMillis();
			stream.forEach(detector);
			long duration = System.currentTimeMillis()-startTime;
			long seconds = (duration) / 1000;
			System.out.println("stream exhausted in "+seconds+" (s)");
		}
		
		//we call an end routine
		this.end();
		
		return results;

	}
	
	public abstract void start();
	
	public abstract void end();

	public abstract F getChangeDetectedBean(TimeWindow window, Block<?> block, double change, Collection<Pattern<B,C>> patterns, long totalTimeMillis, long updateTimeMillis, long descriptionTime);
	
	public abstract F getChangeNotDetectedBean(TimeWindow window, Block<?> block, double change, long totalTimeMillis, long updateTimeMillis, long descriptionTime);
}
