/*******************************************************************************
 * Copyright 2020 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.heuristic.pbcd.boilerplate;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.DecimalFormat;
import java.util.Collection;

import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;

import org.jkarma.pbcd.datastreams.Block;
import org.jkarma.pbcd.datastreams.TimeWindow;
import org.jkarma.pbcd.detectors.PBCD;
import org.jkarma.pbcd.patterns.Pattern;
import org.jkarma.pbcd.patterns.Patterns;
import org.jkarma.heuristic.utils.KarmaArgs;
import org.jkarma.heuristic.utils.ResultBean;
import org.jkarma.mining.joiners.FrequencyEvaluation;
import org.jkarma.model.Transaction;

public class DescriptivePBCDRunner<A extends Transaction<B>, B extends Comparable<B>, C extends FrequencyEvaluation, D, E extends KarmaArgs> 
extends AbstractPBCDRunner<A,B,C,D,E,ResultBean>{
	
	private Writer fileWriter;
	private XMLStreamWriter xml;
	private final DecimalFormat df = new DecimalFormat("#.####");

	
	public DescriptivePBCDRunner(E args, PBCD<A,B,C,D> pbcd) throws IOException, XMLStreamException, FactoryConfigurationError {
		super(args, pbcd);
		
		if(args==null) {
			throw new IllegalArgumentException();
		}
		
		//we open the output xml writer
		File patternFile = this.args.getPatternFile();
		if(patternFile!=null) {
			this.fileWriter = new FileWriter(patternFile);
			this.xml = XMLOutputFactory.newInstance().createXMLStreamWriter(this.fileWriter);
		}
	}
	
	
	
	/**
	 * Dumps to an xml file the input parameters of the execution.
	 */
	@Override
	public void start() {
		if(this.args.getPatternFile()!=null) {
			try {
				//we open the root element
				xml.writeStartDocument();
				xml.writeStartElement("log");
				xml.writeStartElement("params");
				
				//we write the input parameters
				xml.writeStartElement("dataParams");
				xml.writeEmptyElement("filepath");
				xml.writeAttribute("value", this.args.getStreamSource().getAbsolutePath());
				xml.writeEndElement();

				//we write the data stream parameters
				xml.writeStartElement("streamParams");
				xml.writeEmptyElement("blockSize");
				xml.writeAttribute("value", Integer.toString(this.args.getBlockSize()));
				xml.writeEndElement();

				//we write the mining parameters
				xml.writeStartElement("miningParams");
				xml.writeEmptyElement("minimumSupport");
				xml.writeAttribute("value", Float.toString(this.args.getMinSupport()));
				xml.writeEmptyElement("minimumMacroChange");
				xml.writeAttribute("value", Float.toString(this.args.getMinChange()));
				xml.writeEmptyElement("minimumGrowthRate");
				xml.writeAttribute("value", Float.toString(this.args.getMinGr()));
				xml.writeEmptyElement("maximumPatternLength");
				xml.writeAttribute("value", Float.toString(this.args.getMaxDepth()));
				xml.writeEndElement();

				//we close the "params" element
				xml.writeEndElement();
				
				//and we begin the analysis
				xml.writeStartElement("results");
				xml.flush();
			} catch (XMLStreamException e) {
				e.printStackTrace();
			}
		}
	}
	

	@Override
	public ResultBean getChangeDetectedBean(TimeWindow window, Block<?> block, double change,
			Collection<Pattern<B, C>> patterns, long totalTimeMillis, long updateTimeMillis,
			long descriptionTimeMillis) {
		
		long eps = patterns.size();
		long jeps = patterns.stream().mapToDouble(Patterns::getGrowthRate).filter(Double::isInfinite).count();
		double grSum = patterns.stream().mapToDouble(Patterns::getGrowthRate).filter(Double::isFinite).sum();
		
		//we prepare the result bean of this change
		ResultBean bean = new ResultBean();
		bean.changeDetected = 1; //instead of bool we use an int
		bean.startLandmark = window.getFirstInstant().toString();
		bean.endLandmark = window.getLastInstant().toString();
		bean.startBlock = block.getFirstInstant().toString();
		bean.endBlock = block.getLastInstant().toString();
		bean.changeAmount = change;
		bean.sumGrowthRate = grSum;
		bean.totalTime = totalTimeMillis;
		bean.patternUpdateTime = updateTimeMillis;
		bean.patternFilterTime = descriptionTimeMillis;
		bean.epCount = eps;
		bean.jepCount = jeps;
		
		//we extensively dump any relevant information of the change point detected
		//we also dump the descriptive model found
		File patternFile = this.args.getPatternFile();
		if(patternFile!=null) {
			try {
				xml.writeStartElement("changes");
				xml.writeAttribute("in", window.toString());
				xml.writeAttribute("respect", window.getFirstInstant()+", "+block.getLastInstant());
				xml.writeAttribute("amount", Double.toString(change));
				
				for(Pattern<B,C> pattern : patterns) {
					xml.writeStartElement("pattern");
					xml.writeAttribute("growthRate", this.df.format(Patterns.getGrowthRate(pattern)));
					xml.writeAttribute("pastSupport", this.df.format(pattern.getFirstEval().getRelativeFrequency()));
					xml.writeAttribute("updatedSupport", this.df.format(pattern.getSecondEval().getRelativeFrequency()));
					xml.writeStartElement("value");
					xml.writeCharacters(pattern.getItemSet().toString());
					xml.writeEndElement();
					/*
					xml.writeStartElement("evaluationOnLandmark");
					xml.writeAttribute("window", window.toString());
					xml.writeCharacters(pattern.getItemSet().getEval().getAggregate().toString());
					xml.writeEndElement();
					xml.writeStartElement("evaluationOnBlock");
					xml.writeAttribute("window", block.toString());
					xml.writeCharacters(pattern.getItemSet().getEval().getIncrement().toString());
					xml.writeEndElement();
					*/
					xml.writeEndElement();
				}
				xml.writeEndElement();
			}catch(XMLStreamException e) {
				e.printStackTrace();
			}
		}
		
		return bean;
	}

	@Override
	public ResultBean getChangeNotDetectedBean(TimeWindow window, Block<?> block, double change, long totalTimeMillis,
			long updateTimeMillis, long descriptionTimeMillis) {
		//we create a result bean for this change point
		ResultBean bean = new ResultBean();
		bean.changeDetected = 0; //instead of bool an integer
		bean.startLandmark = window.getFirstInstant().toString();
		bean.endLandmark = window.getLastInstant().toString();
		bean.startBlock = block.getFirstInstant().toString();
		bean.endBlock = block.getLastInstant().toString();
		bean.changeAmount = change;
		bean.sumGrowthRate = 0;
		bean.totalTime = totalTimeMillis;
		bean.patternUpdateTime = updateTimeMillis;
		bean.patternFilterTime = descriptionTimeMillis;
		bean.epCount = 0;
		bean.jepCount = 0;
		
		//we extensively dump any related information of the missed change point to file
		File patternFile = this.args.getPatternFile();
		if(patternFile!=null) {
			try {
				xml.writeStartElement("changes");
				xml.writeAttribute("in", window.toString());
				xml.writeAttribute("respect", window.getFirstInstant()+", "+block.getLastInstant());
				xml.writeAttribute("amount", Double.toString(change));
				xml.writeEndElement();
			}catch(XMLStreamException e) {
				e.printStackTrace();
			}
		}
		return bean;
	}

	
	
	/**
	 * Closes the pattern xml dump file at the end of the execution.
	 */
	@Override
	public void end() {
		if(this.args.getPatternFile()!=null) {
			try {
				//we close the result section
				xml.writeEndElement();
				
				//we close the root element
				xml.writeEndElement();
				xml.flush();
				xml.close();
				fileWriter.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (XMLStreamException e1) {
				e1.printStackTrace();
			}
		}
	}

}
