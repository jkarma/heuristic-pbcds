/*******************************************************************************
 * Copyright 2020 Angelo Impedovo
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 ******************************************************************************/
package org.jkarma.heuristic.utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;
import java.util.function.Function;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Stream;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.xml.stream.FactoryConfigurationError;
import javax.xml.stream.XMLStreamException;

import org.jkarma.heuristic.pbcd.boilerplate.DescriptivePBCDRunner;
import org.jkarma.mining.joiners.FrequencyEvaluation;
import org.jkarma.model.LabeledEdge;
import org.jkarma.model.TemporalEdge;
import org.jkarma.model.TemporalGraph;
import org.jkarma.model.Transaction;
import org.jkarma.pbcd.detectors.PBCD;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.OptionHandlerFilter;

public class Utils {
	
	public static <A extends KarmaArgs, B extends Comparable<B>, C extends Transaction<B>, E extends FrequencyEvaluation, F, D extends PBCD<C,B,E,F>> 
	void parseArgsAndExecute(String[] cmdArgs, Class<A> argsType, Function<A,Stream<C>> sourceLoader, Function<A,D> pbcdBuilder) 
	throws InstantiationException, IllegalAccessException {

		Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);
		final A args = argsType.newInstance();
		final CmdLineParser argsParser = new CmdLineParser(args);
		final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		final Validator validator = factory.getValidator();
		
		try {
			argsParser.parseArgument(cmdArgs);
			Set<ConstraintViolation<A>> violations = validator.validate(args);
			if(violations.isEmpty()) {
				//we open the data source
				Stream<C> stream = sourceLoader.apply(args);
				//we build the detector
				PBCD<C,B,E,F> pbcd = pbcdBuilder.apply(args);
				//we inject stream and detector into the runner before running the computation
				Vector<ResultBean> results = new DescriptivePBCDRunner<C,B,E,F,A>(args,pbcd).apply(stream);
				//we save the results
				CsvBeanUtils.saveStream(args.getOutputFile(), ResultBean.class, results.stream());
			}else {
				for(ConstraintViolation<A> violation : violations) {
					System.err.println("invalid argument: "+violation.getPropertyPath()+" "+violation.getMessage());
				}
			}
		} catch (CmdLineException e) {
			System.err.println(e.getMessage());
			System.err.println("java SampleMain [options...] arguments...");
			argsParser.printUsage(System.err);
			System.err.println();
			System.err.println(" Example: java SampleMain"+
				argsParser.printExample(OptionHandlerFilter.ALL, null)
			);	
		} catch (FactoryConfigurationError e ) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public static <A extends KarmaArgs> Stream<TemporalGraph> getTemporalGraphStream(A args){
		try {
			return Utils.getTemporalGraphStream(args.getStreamSource());
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	

	public static Stream<TemporalGraph> getTemporalGraphStream(File csv) throws FileNotFoundException {
		Stream<TemporalGraph> graphStream = null;
		
		//we open the data source as a stream of temporal edges
		Stream<TemporalEdge> dataStream = CsvBeanUtils.parseStream(csv, TemporalEdge.class);
		
		//we produce a secondary stream of temporal graphs by aggregating in a same graph the temporal edges
		//having same timestamp.
		graphStream = dataStream.map(new Function<TemporalEdge,TemporalGraph>(){
			
			private Instant lastTimestamp;
			private int transactionId = 0;
			private HashSet<LabeledEdge> edges;

			@Override
			public TemporalGraph apply(TemporalEdge edge) {
				Instant edgeInstant = edge.timestamp.toInstant();
				TemporalGraph snapshot = null;

				if(this.lastTimestamp==null || this.lastTimestamp.isBefore(edgeInstant)){
					if(this.lastTimestamp!=null) {
						snapshot = new TemporalGraph(this.transactionId, this.lastTimestamp, this.edges);
					}
					this.edges = new HashSet<>();
					this.transactionId++;
					this.lastTimestamp = edgeInstant;
				}

				this.edges.add(edge);
				return snapshot;
			}

		}).filter(graph -> graph!=null);
		
		return graphStream;
	}
 
}
